# WebLog

This application is a solution to the weblog "parallel job processing" challenge.  This was presented as Burst Java Assessment.
Basic idea is to parse a (potentially giant) weblog file and count which "base URLs" have received the most hits.
The intent is to implement this using "parallel processing" to illustrate this technical skill
and (hopefully) also improve performance (TBD)

## Installation and Getting Started

1. Clone the repo from Bitbucket.
2. Ensure you have Java installed locally.  Compile the code.
3. If needed, generate a dummy weblog file:  `java com.craigmilley.weblog.TestFileGenerator <OutputFile> <size>` where size is something like "500mb" or "3G"
4. Run the processor to count the top 5 hits:  `java com.craigmilley.weblog.WebLogProcessor <InputFile> (<ChunkSize) (WorkerThreads)`
5. This call should emit a file which is a sibling of the original file: `<inputfile>.summary`

## Dependencies

System makes very little use of 3rd party libraries.  All that is needed is Java.  I used JDK 1.12
This was by design in order to make this easiest to build/compile.

## Basic design

Basic classes in the system:

1. **Splitter**:  Splits the file into chunks.  Chunk size is configurable.  Currently chunk size will usually slightly 
exceed the configured chunk size because code makes an effort to split along newlines instead of in the middle of a line.

2. **WebLogProcessor**: Main entry point.  Offers a `parse(String inputFile)` method which does the work.  Splits file 
into chunks.  Creates a pool of worker threads to process the chunks in parallel.  Consolidates
outputs of threads and creates master summary file at end.

3. **WorkerThread**: Processes a single chunk file.  Reads file line by line.  Tries to parse each
line as a URL. If fails, line is skipped and silently ignored.  Normalizes URL to "base URL".
e.g. www.burst.com?foo=bar counts as a click for "burst.com"   

Thread pool is implemented using a built-in Java "fixed size thread pool".
See https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html for details.

## Bonus Questions

1. If the number of slaves in the pool is less than the number of file parts, how would you do the scheduling?
We get this for free using ExecutorService.  The ExecutorService functions as a queue.  We can add all
chunks to the queue using method https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html#submit(java.lang.Runnable)
The tasks will remain in the queue.  When worker threads finish processing one chunk, they will take
next chunk from queue.

2. Handle errors reported by slaves, and restart jobs for slaves that failed. You will have to maintain state of which jobs finished.
Currently master thread is not handling errors.  Theoretically, master thread could hold onto the 
`Future` object returned by each call to `ExecutorService.submit()`.  It could then poll the Future
objects to see if any tasks have failed and restart them.

## How long did you spend on this exercise?

~4 hours

## TODO

* Implement error handling.  Currently worker thread errors are swallowed.
* Performance benchmarking
* Unit tests
* Proper handling for small files (skip the chunking and operate on original file)
* Proper build system (e.g. Maven)
* Improve splitting so that chunk files are not bigger than configured size.
* Proper logging using SL4j

## Authors

* **Craig Milley** - *All work* - [Bitbucket Repo](https://bitbucket.org/craig_milley/WebLog)