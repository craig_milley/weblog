package com.craigmilley.weblog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Worker thread class which processes a single chunk of a web log and emits a summary file.
 *
 * @author cmilley
 */
public class WorkerThread implements Runnable {

    /**
     * Class representing status of WorkerThread.  Currently unused.
     */
    enum Status {
        NOT_STARTED,
        RUNNING,
        FINISHED_SUCCESS,
        FINISHED_WITH_ERROR
    }

    private File inputFile;
    private Map<String, Integer> hostCounts = new HashMap<>();
    private long validLines = 0;
    private long invalidLines = 0;
    private Status status = Status.NOT_STARTED;

    /**
     * Create a worker thread object for processing a single chunk file.
     */
    public WorkerThread(File inputFile) {
        this.inputFile = inputFile;
    }

    /**
     * Process the chunk file line by line.
     */
    @Override
    public void run() {
        log("processing file " + inputFile.getName());
        status = Status.RUNNING;

        // Process file line by line.
        // Use scanner to read file.  We must NOT load entire file into memory.
        try (FileInputStream inputStream = new FileInputStream(inputFile);
             Scanner sc = new Scanner(inputStream, "UTF-8")) {
            while (sc.hasNextLine()) {
                processLine(sc.nextLine());
            }

            // Write a summary file.
            File summaryFile = writeSummary();
            status = Status.FINISHED_SUCCESS;

            log("Complete.  Summary file: " + summaryFile.getAbsolutePath());

        } catch (IOException ex) {
            ex.printStackTrace();
            status = Status.FINISHED_WITH_ERROR;
        }
    }

    /**
     * Process a single line of the web log chunk.  A line that is not a valid URL will be considered an invalid line
     * and invalidLines will be incremented.  A line with a valid URL will be processed, host will be determined, and
     * count will be incremented in internal map for that host.
     */
    private void processLine(String line) {
        line = line.trim();
        if (line.length() == 0) {
            return;
        }
        try {
            URL url = new URL(line);
            String host = normalizeHost(url.getHost());
            if (host == null) {
                invalidLines++;
                return;
            }
            Integer count = hostCounts.get(host);
            if (count == null) {
                count = 0;
            }

            hostCounts.put(host, ++count);
        } catch (MalformedURLException ex) {
            invalidLines++;
        }
    }

    /**
     * Note:  Part of the requirements are that we only track base URL which is defined as top level domain and
     * subdomain immediately in front of top level domain.  Therefore:
     * <pre>
     *     www.burst.com -> burst.com
     *     ftp.burst.com -> burst.com
     *     www.subdomain.burst.com -> burst.com
     * </pre>
     */
    private String normalizeHost(String host) {
        int index = host.lastIndexOf('.');
        if (index == -1) {
            return null;
        }
        index = host.lastIndexOf('.', index - 1);
        if (index == -1) {
            return null;
        }
        return host.substring(index + 1);
    }

    /**
     * Write the map of URL counts to a summary file, e.g.
     * <pre>
     *     elastic.co 425
     *     google.com 1500
     *     burst.com 1250
     *     amazon.com 1500
     *     harness.io 800
     * </pre>
     * Note:  order does not matter here.  Master process will consolidate results and apply the ordering.
     */
    private File writeSummary() throws IOException {
        log("writing summary file");
        File summaryFile = WebLogProcessor.getSummaryFile(inputFile);
        try (FileWriter writer = new FileWriter(summaryFile)) {
            for (String key : hostCounts.keySet()) {
                writer.write(key + " " + hostCounts.get(key) + "\n");
            }
        }
        return summaryFile;
    }

    /**
     * Write a log message with thread name as prefix for debugging purposes.
     */
    private void log(String msg) {
        System.out.println(Thread.currentThread().getName() + ": " + msg);
    }
}
