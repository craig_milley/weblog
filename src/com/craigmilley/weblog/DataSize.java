package com.craigmilley.weblog;

/**
 * Simple utility class for mapping logical data size (e.g. "10mb") to actual byte counts.
 *
 * @author cmilley
 */
public class DataSize {

    public static final long MB = 1024l * 1024l;
    public static final long GB = 1024l * MB;

    /**
     * Convert logical data size (e.g. "10mb") to actual byte counts. Currently supported:  Megabytes (m or mb) and
     * gigabytes (g or gb).  Case insentive.  All invalid string sizes will result in thrown runtime exception.
     *
     * @param size stringified size, e.g. 100mb or 5G or 100000.  Case insentive.  Must represent a positive number.
     * @return byte count
     */
    public static long toByteCount(String size) {
        size = size.toLowerCase();
        long byteCount;

        if (size.endsWith("mb")) {
            byteCount = MB * Integer.parseInt(size.substring(0, size.length() - 2));
        } else if (size.endsWith("gb")) {
            byteCount = GB * Integer.parseInt(size.substring(0, size.length() - 2));
        } else if (size.endsWith("m")) {
            byteCount = MB * Integer.parseInt(size.substring(0, size.length() - 1));
        } else if (size.endsWith("g")) {
            byteCount = GB * Integer.parseInt(size.substring(0, size.length() - 1));
        } else {
            byteCount = Integer.parseInt(size);
        }
        if (byteCount < 1) {
            throw new IllegalArgumentException("Invalid size " + size + ". Must be positive");
        }
        return byteCount;
    }
}
