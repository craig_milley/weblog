package com.craigmilley.weblog;

import java.io.*;
import java.util.Random;

/**
 * Quick and dirty helper class to generate a file with a given size with a randomized URL on each line.
 *
 * @author cmilley
 */
public class TestFileGenerator {

    private static Random random = new Random(System.currentTimeMillis());
    private static String[] urls = {
            "burst.com",
            "google.com",
            "elastic.co",
            "harness.io",
            "salesforce.com",
            "demandware.com",
            "mineraltree.com"
    };

    /**
     * Generate a test web log file.
     * <pre>
     *     Syntax: TestFileGenerator <OutputFile> <size>
     * </pre>
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            throw new RuntimeException("Syntax: TestFileGenerator <OutputFile> <size>");
        }

        // Slurp arguments.
        String outputFilename = args[0];
        long bytes = DataSize.toByteCount(args[1]);

        File outputFile = new File(outputFilename);
        System.out.println("Writing " + args[1] + " worth of web logs to " + outputFile.getAbsolutePath());

        long bytesWritten = 0;
        try (FileOutputStream fos = new FileOutputStream(outputFile);
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos))) {
            while (bytesWritten < bytes) {
                String url = getRandomURL();
                writer.write(url + "\n");
                bytesWritten += url.length();
            }
        }
        System.out.println("Generated test file of size " + outputFile.length() + " bytes");
    }

    /**
     * Get random URL of the form www.<baseUrl>?<querystring 0-254 chars)
     */
    static String getRandomURL() {
        int n = random.nextInt(urls.length);
        int queryStringLen = random.nextInt(255);
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i <= queryStringLen; i++) {
            char c = (char) ('a' + random.nextInt(26));
            builder.append(c);
        }
        return "http://www." + urls[n] + "?" + builder.toString();
    }

}
