package com.craigmilley.weblog;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Package level helper class which splits a file into chunks of a given size.
 * <p/>
 * Splitting is done along new line and so chunks will be slightly larger than configured size.
 *
 * @author cmilley
 */
class Splitter {

    public static final long DEFAULT_CHUNK_SIZE = 100 * DataSize.MB;

    private final long chunkSize;

    /**
     * Construct a splitter with default chunk size.
     */
    Splitter() {
        this(DEFAULT_CHUNK_SIZE);
    }

    /**
     * Constructor a splitter with the supplied chunk size.
     *
     * @param chunkSize chunk size in bytes.
     */
    Splitter(long chunkSize) {
        if (chunkSize < 1) {
            throw new IllegalArgumentException("Invalid chunk size " + chunkSize);
        }
        this.chunkSize = chunkSize;
    }

    /**
     * Split input file into chunks.  The files will be slightly larger than configured chunk size because we do not
     * want to split in the middle of a line.  Files will be stored in a subdirectory of the directory containing the
     * passed file.  Subdirectory name will be random.  File names will be of the form: "<input filename>.<index>.part"
     */
    List<File> split(File inputFile) throws IOException {
        // Use subdirectory with random name as temporary processing folder
        File dir = inputFile.getParentFile();
        UUID jobId = UUID.randomUUID();
        File subdir = new File(dir, jobId.toString());
        subdir.mkdirs();

        // Calculate chunks
        long fileSize = inputFile.length();
        long numChunks = (fileSize / chunkSize) + 1;

        System.out.println("Splitting input file [" + inputFile.getAbsolutePath() + "] into chunks");
        List<File> chunks = new ArrayList<>();
        long sumChunkSizes = 0;

        try (RandomAccessFile raf = new RandomAccessFile(inputFile, "r");
             FileChannel inputChannel = raf.getChannel()) {
            long startPos = 0;
            for (int i = 0; i < numChunks; i++) {
                File chunkFile = getChunkFile(inputFile.getName(), subdir, i);
                chunks.add(chunkFile);
                long endPos = Math.min(chunkSize * (i + 1), fileSize);
                raf.seek(endPos);

                // read until hitting newline or EOF so that we do not split in middle of a line
                int c;
                while ((c = raf.read()) != '\n' && c != -1) {
                    endPos++;
                }
                createPart(inputChannel, chunkFile, startPos, endPos);
                startPos = endPos + 1;
                sumChunkSizes += chunkFile.length();
            }
        }

        // validate chunk sizes
        System.out.println("Original file size: " + fileSize);
        System.out.println("Chunk files size: " + sumChunkSizes);

        return chunks;
    }

    /**
     * Create a chunk from given inputChannel with the number of bytes represented by startPos, endPos.
     */
    private static void createPart(FileChannel inputChannel, File chunkFile, long startPos, long endPos) throws IOException {
        try (RandomAccessFile outputRaf = new RandomAccessFile(chunkFile, "rw");
             FileChannel outputChannel = outputRaf.getChannel()) {
            inputChannel.position(startPos);
            outputChannel.transferFrom(inputChannel, 0, endPos - startPos + 1);
        }
    }

    /**
     * Get filename used for chunks.
     */
    static File getChunkFile(String filename, File subdir, int i) {
        return new File(subdir, filename + "." + i + ".part");
    }
}
